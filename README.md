### 简介
    pet-life是一个宠物社区小程序

    主要功能有：
        宠物领养、寻宠、宠物社区、签到答题、对话咨询等。

### 核心技术
#### 依赖
- mysql5.7
- jdk1.8
- node.js14
#### 前端
- uniapp
- npm
- vue
- element-ui
#### 后端
- springboot
- spring security
- redis
- mybatis
- mybatis-plus
- fastjson
- jwt

### 小程序截图
|![输入图片说明](imgs/DF3D15EF052DDC53674240C6940FD8A4.jpg)|![输入图片说明](imgs/8F0E8E9BD0189F07233759EC0D5ACC1E.jpg)|![输入图片说明](imgs/74C88C49D11715596A493C9F355A0BDA.jpg)|![输入图片说明](imgs/CCB0B465E7899C177114BF637855005D.jpg)|
|---|---|---|---|
|![输入图片说明](imgs/950B1BCB0D7BC2F8B768B40003C445FE.jpg)|![输入图片说明](imgs/C5DA1D858766518EDDA70213B0209389.jpg)|![输入图片说明](imgs/0DBA62F1FFCFF7ADE136AEB365D6B4C6%20-%20%E5%89%AF%E6%9C%AC.jpg)|![输入图片说明](imgs/3C2B0636550EBEF8070EFECD45C52AEF.jpg)|
|---|---|---|---|
|![输入图片说明](imgs/76DCFD3438255FE4E813B221AB4F2384.jpg)|![输入图片说明](imgs/CD31D18331CCFBB5D4FEAD22EF829125.jpg)|![输入图片说明](imgs/EBCEB148A28ECCE784F11D390D0B5F34.jpg)|![输入图片说明](imgs/2D40AEA666CFE5085044803DD9AB6749.jpg)|

### 联系作者
 **微信：TL07061030** （备注gitee）

或关注公众号 "连帆起航"，加作者微信

注：源码收费哦！购买源码后，你可获得作者指导搭建、代码咨询，以及项目中的任何疑问！

![输入图片说明](imgs/gzh.png)

